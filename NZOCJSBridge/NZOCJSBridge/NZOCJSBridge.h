//
//  NZOCJSBridge.h
//  NZOCJSBridge
//
//  Created by zhao on 15/6/23.
//  Copyright (c) 2015年 nerozhao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIWebView.h>

#define kCustomProtocolScheme @"nzjsscheme"
#define kQueueHasMessage @"__NZJS_QUEUE_MESSAGE__"

typedef void (^NZJSResponseCallback)(id responseData);
typedef void (^NZJSHandler)(id data, NZJSResponseCallback responseCallback);

@interface NZOCJSBridge : NSObject<UIWebViewDelegate>

+ (void)enableLogging;

+(instancetype)bridgeForWebView:(UIWebView *)webView webViewDelegate:(NSObject<UIWebViewDelegate>*)webViewDelegate handler:(NZJSHandler)handler;
- (void)send:(id)message responseCallback:(NZJSResponseCallback)responseCallback;

@end
