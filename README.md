#NZOCJSBridge
项目是为混合开发提供Objective-C与Javascript通信的中间件

只需将目录NZOCJSBridge拷贝到自己项目中，项目中自带Demo，使用前请熟悉例子

为方便阅读与使用，只提供两个主要的api，有需要可自行扩展

一、oc向js发送消息并回调
```
[_bridge send:@"OC send String to JS" responseCallback:^(id response) {
        NSLog(@"OC got response: %@", response);
    }];
```
二、js向oc发送消息并回调
```
bridge.send('JS send String to OC', function(responseData) {
    console.log('JS got response', responseData)
})
```
注:只项目为WebViewJavascriptBridge的缩略版